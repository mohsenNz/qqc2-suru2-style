### Installation

Note: Qt 5.12 or higher needed

Go to the root directory of the project and run:

```
mkdir build; cd build
qmake ../suru2.pro
make
make install
```

for unistall run:

```
make uninstall
```