/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

T.SwitchDelegate {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           Math.max(contentItem.implicitHeight, indicator ? indicator.implicitHeight : 0) +
                           topPadding + bottomPadding)

  baselineOffset: contentItem.y + contentItem.baselineOffset

  padding         : control.Suru2.units.gu(2)
  topPadding      : padding - 2
  bottomPadding   : padding - 2


  spacing: control.Suru2.units.gu(1.5)

  opacity: control.enabled ? 1.0 : 0.5

  font: Suru2.units.fontParagraph

  indicator: SwitchIndicator {
    x: text ?
         (control.mirrored ?
            control.leftPadding :
            control.width - width - control.rightPadding) :
         control.leftPadding + (control.availableWidth - width) / 2

    y: control.topPadding + (control.availableHeight - height) / 2

    control: control
  }

  contentItem: Text {
    leftPadding: !control.mirrored ? 0 : control.indicator.width + control.spacing
    rightPadding: control.mirrored ? 0 : control.indicator.width + control.spacing

    text    : control.text
    font    : control.font
    color   : control.Suru2.foregroundColor
    elide   : Text.ElideRight
    visible : control.text

    horizontalAlignment: Text.AlignLeft
    verticalAlignment: Text.AlignVCenter
  }

  background: Rectangle {
    implicitHeight: control.Suru2.units.gu(7)
    color: control.Suru2.backgroundColor

    HighlightFocusRectangle {
      control: control
      width: parent.width
      height: parent.height
    }

    Rectangle {
      anchors.bottom: parent.bottom
      width: parent.width
      height: 1
      color: control.Suru2.secondarySplitColor
    }
  }
}
