/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Controls.Suru2   2.4

Rectangle {
  id: cursor

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  color   : control.Suru2.accentColor
  width   : 2
  visible : parent.activeFocus && !parent.readOnly && parent.selectionStart ===
            parent.selectionEnd

  Connections {
    target: cursor.parent

    onCursorPositionChanged: {
      // keep a moving cursor visible
      cursor.opacity = 1
      timer.restart()
    }
  }

  Timer {
    id: timer

    running : cursor.parent.activeFocus && !cursor.parent.readOnly
    repeat  : true
    interval: Qt.styleHints.cursorFlashTime / 2

    onTriggered: cursor.opacity = !cursor.opacity ? 1 : 0
    // force the cursor visible when gaining focus
    onRunningChanged: cursor.opacity = 1
  }
}
