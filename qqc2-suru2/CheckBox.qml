/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls         2.4
import QtQuick.Controls.impl    2.4
import QtQuick.Controls.Suru2   2.4

T.CheckBox {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  property bool useSystemFocusVisuals: true

  implicitWidth   : Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight  : Math.max(background ? background.implicitHeight : 0,
                           Math.max(contentItem.implicitHeight, indicator ? indicator.implicitHeight : 0) +
                             topPadding + bottomPadding)

  baselineOffset: contentItem.y + contentItem.baselineOffset

  padding : Suru2.units.gu(0.5)
  spacing : Suru2.units.gu(1.5)
  font    : Suru2.units.fontParagraph
  opacity : control.enabled ? 1.0 : 0.5

  indicator: CheckIndicator {
    x: text ? (control.mirrored ? control.width - width - control.rightPadding : control.leftPadding)
            : control.leftPadding + (control.availableWidth - width) / 2

    y: control.topPadding + (control.availableHeight - height) / 2

    control: control
  }

  contentItem: CheckLabel {
    leftPadding: control.indicator && !control.mirrored ?
                   control.indicator.width + control.spacing : 0

    rightPadding: control.indicator && control.mirrored ?
                    control.indicator.width + control.spacing : 0

    text    : control.text
    font    : control.font
    elide   : Text.ElideRight
    visible : control.text
    color   : control.Suru2.foregroundColor

    horizontalAlignment : Text.AlignLeft
    verticalAlignment   : Text.AlignVCenter

  }
}
