/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                2.11
import QtQuick.Controls       2.4
import QtQuick.Controls.impl  2.4
import QtQuick.Templates      2.4   as T
import QtQuick.Controls.Suru2 2.4

T.BusyIndicator {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  } 

  implicitWidth   : contentItem.implicitWidth + leftPadding + rightPadding
  implicitHeight  : contentItem.implicitHeight + topPadding + bottomPadding

  contentItem: IconImage {
    id: image

    sourceSize.width  : control.Suru2.units.gu(6)
    sourceSize.height : control.Suru2.units.gu(6)

    width   : control.Suru2.units.gu(6)
    height  : control.Suru2.units.gu(6)

    source  : "qrc:/assets/spinner.svg"
    opacity : running ? 1.0 : 0.0
    color   : control.Suru2.accentColor

    RotationAnimator on rotation {
        running   : control.visible && control.running
        from      : 0
        to        : 360
        loops     : Animation.Infinite
        duration  : control.Suru2.sleepyDuration
    }

  }
}
