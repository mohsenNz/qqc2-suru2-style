/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls         2.4
import QtQuick.Controls.Suru2   2.4

Item {
  id: indicator

  Suru2.theme         : control.Suru2.theme
  Suru2.highlightType : control.Suru2.highlightType

  property Item control

  implicitWidth: control.Suru2.units.gu(5)
  implicitHeight: control.Suru2.units.gu(2.5)

  Rectangle {
    id: backgroundRec

    width   : parent.width
    height  : parent.height
    radius  : height / 2
    color   : control.checked ?
                control.Suru2.checkColor :
                control.Suru2.theme === Suru2.Light ?
                  control.Suru2.tertiaryBackgroundColor :
                  control.Suru2.backgroundColor

    border.width: 1
    border.color: control.checked ? control.Suru2.checkColor : control.Suru2.theme === Suru2.Light ?
                    control.Suru2.tertiaryBackgroundColor : Qt.darker(control.Suru2.splitColor, 1.5)

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }

    Behavior on border.color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }

  //Row {
      //width: parent.width
      //height: parent.height

      //Label {
          //width   : parent.width * 0.5
          //height  : parent.height
          //text    : "I"
          //color   : indicator.Suru2.backgroundColor

          //horizontalAlignment: Label.AlignHCenter
          //verticalAlignment: Label.AlignVCenter
      //}

      //Label {
          //width   : parent.width * 0.5
          //height  : parent.height
          //text    : "O"
          //color   : Qt.darker(backgroundRec.color, 1.3)
          //horizontalAlignment: Label.AlignHCenter
          //verticalAlignment: Label.AlignVCenter
      //}
  //}

  Rectangle {
    width   : control.Suru2.units.gu(2.5) - (control.Suru2.theme === Suru2.Light ? 6 : 4)
    height  : control.Suru2.units.gu(2.5) - (control.Suru2.theme === Suru2.Light ? 6 : 4)
    radius  : height / 2
    color   : control.Suru2.theme === control.Suru2.Light ?
                control.Suru2.backgroundColor : control.Suru2.secondaryBackgroundColor

    border.width: 1
    border.color:  control.Suru2.theme === control.Suru2.Light ?
                    control.Suru2.backgroundColor : control.Suru2.tertiaryBackgroundColor

    layer.enabled: control.Suru2.theme === control.Suru2.Light
    layer.effect: ElevationEffect {
      elevation: control.enabled ? 2 : 0
    }

    readonly property real __space: (control.Suru2.theme === Suru2.Light ? 3 : 2)

    x: Math.max(__space, Math.min(parent.width - width - __space,
                            control.visualPosition * parent.width - (width / 2)))

    y: (parent.height - height) / 2

    Behavior on x {
      enabled: !control.pressed
      NumberAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InOutCubic
      }
    }
  }
}

