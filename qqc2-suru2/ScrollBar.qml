/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

T.ScrollBar {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           contentItem.implicitHeight + topPadding + bottomPadding)

  visible: control.policy !== T.ScrollBar.AlwaysOff

  // TODO: arrows

  contentItem: Item {
    implicitWidth: control.interactive ? control.Suru2.units.gu(1.2) : 3
    implicitHeight: control.interactive ? control.Suru2.units.gu(1.2) : 3

    Rectangle {
      id: itemRec

      anchors.fill: parent
      anchors.margins: control.hovered || control.pressed  ? 2 : 7
      anchors.leftMargin: control.orientation === Qt.Horizontal ? 2 : undefined
      anchors.topMargin: control.orientation === Qt.Vertical ? 2 : undefined
      anchors.bottomMargin: 2
      anchors.rightMargin: 2

      radius: 6
      color: control.pressed ?
               Qt.darker(control.Suru2.accentColor, 1.2) :
               control.interactive && control.hovered ?
                 Qt.darker(control.Suru2.accentColor, 1.1) :
                 control.Suru2.accentColor

      Behavior on anchors.margins {
        SequentialAnimation {
          PauseAnimation { duration: itemRec.anchors.margins === 2 ? 3000 : 0 }
          NumberAnimation {
            duration: Suru2.briskDuration
            easing.type: Easing.OutCubic
          }
        }
      }
    }

    opacity: 0.0
//        radius: 10
  }

  background: Rectangle {
    id: backgroundRec

    implicitWidth: control.interactive ? control.Suru2.units.gu(1.5) : 0

    color   : control.Suru2.secondaryBackgroundColor
    visible : control.size < 1.0
    opacity : control.hovered || control.pressed ? 0.8 : 0

    Behavior on opacity {
      SequentialAnimation {
        PauseAnimation { duration: backgroundRec.opacity === 0.8 ? 3000 : 0 }
        NumberAnimation {
          duration: Suru2.briskDuration
          easing.type: Easing.OutCubic
        }
      }
    }
  }

  states: [
    State {
      name: "active"
      when: control.policy === T.ScrollBar.AlwaysOn || (control.active && control.size < 1.0)
    }
  ]

  transitions: [
    Transition {
      to: "active"
      ParallelAnimation {
        NumberAnimation { targets: contentItem; property: "opacity"; to: 1.0;
          duration: control.Suru2.briskDuration; easing.type: Easing.OutCubic }

//                NumberAnimation { targets: background; property: "opacity"; to: 0.8; duration: control.Suru2.briskDuration; easing.type: Easing.InCubic }
      }
    },
    Transition {
      from: "active"
      SequentialAnimation {
        PauseAnimation { duration: 3000 }
        NumberAnimation { targets: contentItem; property: "opacity"; to: 0.0;
          duration: control.Suru2.briskDuration; easing.type: Easing.OutCubic }
      }
    }
  ]
}
