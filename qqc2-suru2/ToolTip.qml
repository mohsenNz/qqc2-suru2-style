/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

T.ToolTip {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  x: parent ? (parent.width - implicitWidth) / 2 : 0
  y: -implicitHeight - control.Suru2.units.gu(2)

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           contentItem.implicitHeight + topPadding + bottomPadding)

  margins: control.Suru2.units.gu(1)
  padding: control.Suru2.units.gu(1)

  font: Suru2.units.fontParagraph

  closePolicy: T.Popup.CloseOnEscape |
               T.Popup.CloseOnPressOutsideParent |
               T.Popup.CloseOnReleaseOutsideParent

  contentItem: Text {
      text    : control.text
      font    : control.font
      color   : control.Suru2.backgroundColor
      // TODO: wrapMode: Label.Wrap
  }

  enter: Transition {
    NumberAnimation {
      property    : "opacity"
      from        : 0.0
      to          : 1.0
      easing.type : Easing.InOutCubic
      duration    : control.Suru2.briskDuration
    }
  }

  exit: Transition {
    NumberAnimation {
      property    : "opacity"
      from        : 1.0
      to          : 0.0
      duration    : control.Suru2.briskDuration
      easing.type : Easing.InOutCubic
    }
  }

  background: Rectangle {
    color   : control.Suru2.foregroundColor
    radius  : 4
    opacity : 0.85    // Match opacity of overlayColor

    layer.enabled: true
    layer.effect: ElevationEffect {
      elevation: 3
    }
  }
}
