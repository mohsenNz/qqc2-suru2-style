/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Controls         2.4
import QtQuick.Controls.impl    2.4
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

T.ToolButton {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           contentItem.implicitHeight + topPadding + bottomPadding)

  baselineOffset: contentItem.y + contentItem.baselineOffset

  padding: control.Suru2.units.gu(0.5)

  icon.width  : 16
  icon.height : 16
  icon.color  : (control.checked || control.highlighted) ?
                  control.Suru2.accentColor : control.Suru2.foregroundColor

  property bool useSystemFocusVisuals: true

  font: Suru2.units.fontParagraph

  opacity: control.enabled ? 1.0 : 0.5

  contentItem: IconLabel {
    text      : control.text
    font      : control.font
    spacing   : control.spacing
    mirrored  : control.mirrored
    display   : control.display
    icon      : control.icon
    color     : (control.checked || control.highlighted) ?
                control.Suru2.accentColor : control.Suru2.foregroundColor

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }

  background: Rectangle {
    implicitWidth: control.Suru2.units.gu(4.5)
    implicitHeight: control.Suru2.units.gu(4.5)

    radius  : Math.min(width, height) / 4
    visible : control.down || control.hovered || control.pressed
    color   : control.down ? Qt.darker(control.Suru2.secondaryBackgroundColor, 1.2)
                           : control.Suru2.secondaryBackgroundColor

//    opacity: control.down ? 1 : 0.5

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }
}
