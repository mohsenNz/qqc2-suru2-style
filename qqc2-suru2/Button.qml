/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                2.11
import QtQuick.Controls       2.4
import QtQuick.Controls.impl  2.4
import QtQuick.Templates      2.4   as T
import QtQuick.Controls.Suru2 2.4

T.Button {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }
  
  implicitWidth: Math.max(background ? background.implicitWidth : 0 ,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0 ,
                           contentItem.implicitHeight + topPadding + bottomPadding)

  baselineOffset: contentItem.y + contentItem.baselineOffset

  padding       : control.Suru2.units.gu(1)
  leftPadding   : control.Suru2.units.gu(2)
  rightPadding  : control.Suru2.units.gu(2)

  spacing     : Suru2.units.gu(0.5)
  font        : Suru2.units.fontParagraph

  icon.width  : 24
  icon.height : 24
  icon.color  :
      control.flat && control.highlighted ?
        control.Suru2.accentColor :
        (control.highlighted ?
           control.Suru2.backgroundColor :
           control.Suru2.highlightType === Suru2.None ?
             control.Suru2.foregroundColor : "white")

  property bool useSystemFocusVisuals: true

  opacity: control.enabled ? 1.0 : 0.5

  contentItem: IconLabel {
    spacing   : control.spacing
    mirrored  : control.mirrored
    display   : control.display
    text      : control.text
    font      : control.font
    icon      : control.icon
    scale     : control.down ? 0.97 : 1

    color: control.flat && control.highlighted ?
             control.Suru2.accentColor :
             (control.highlighted ?
                control.Suru2.backgroundColor :
                control.Suru2.highlightType === Suru2.None ?
                  control.Suru2.foregroundColor :
                  "white")

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }

  background: Rectangle {
    implicitWidth   : control.Suru2.units.gu(8)
    implicitHeight  : control.Suru2.units.gu(3)

    property color __baseColor: {
      if (control.flat)
        return control.Suru2.backgroundColor

      switch (control.Suru2.highlightType) {
      case Suru2.Positive:
        return control.Suru2.positiveColor
      case Suru2.Negative:
        return control.Suru2.negativeColor
      case Suru2.Warning:
        return control.Suru2.warningColor
      case Suru2.Info:
        return control.Suru2.accentColor
      case Suru2.None:
        if (control.highlighted || control.checked) {
          return control.Suru2.accentColor
        } else {
          return control.Suru2.tertiaryBackgroundColor
        }
      }
    }

    color: control.down ?
             Qt.darker(__baseColor, 1.3) :
             control.hovered ?
               Qt.darker(__baseColor, 1.1) :
               __baseColor


    radius: 6

    border.width: (control.highlighted || control.checked || control.flat ||
                   control.Suru2.highlightType !== Suru2.None) ? 0 : 1

    border.color: control.Suru2.splitColor

    layer.enabled: (control.highlighted || control.checked) && !control.flat
    layer.effect: ElevationEffect {
      elevation: !control.down ? 2 : 3
    }

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }

  Behavior on icon.color {
    ColorAnimation {
      duration: control.Suru2.fastDuration
      easing.type: Easing.InCubic
    }
  }

}
