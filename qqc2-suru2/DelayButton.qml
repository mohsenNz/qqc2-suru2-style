/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

T.DelayButton {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           contentItem.implicitHeight + topPadding + bottomPadding)

  baselineOffset: contentItem.y + contentItem.baselineOffset

  leftPadding     : control.Suru2.units.gu(2)
  rightPadding    : control.Suru2.units.gu(2)
  topPadding      : control.Suru2.units.gu(1)
  bottomPadding   : control.Suru2.units.gu(1)

  property bool useSystemFocusVisuals: true

  font   : Suru2.units.fontParagraph
  opacity: control.enabled ? 1.0 : 0.5

  transition: Transition {
    NumberAnimation {
      duration: control.delay * (control.pressed ? 1.0 - control.progress : 0.3 * control.progress)
    }
  }

  contentItem: Text {
    text    : control.text
    font    : control.font
    elide   : Text.ElideRight

    horizontalAlignment : Text.AlignHCenter
    verticalAlignment   : Text.AlignVCenter

    color: control.checked ? control.Suru2.backgroundColor : control.Suru2.foregroundColor

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }

  background: Rectangle {
    id: controlBg

    implicitWidth   : control.Suru2.units.gu(8)
    implicitHeight  : control.Suru2.units.gu(4)

    property color __normalColor: control.checked ? control.Suru2.accentColor
                                                  : control.Suru2.tertiaryBackgroundColor

    color: control.down ? Qt.darker(__normalColor, 1.2)
                        : control.hovered ? Qt.darker(__normalColor, 1.1) : __normalColor
    radius: 4

    border.width: control.checked ? 0 : 1
    border.color: control.Suru2.splitColor

    Item {
      width: parent.width * control.progress
      height: 3

      anchors.bottom: parent.bottom

      clip: true

      Rectangle {
        visible : !control.checked
        width   : parent.width + (control.progress == 1.0 ? 0 : controlBg.radius)
        height  : parent.height + controlBg.radius
        radius  : controlBg.radius
        color   : control.Suru2.accentColor

        anchors.bottom: parent.bottom
      }
    }

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }
}
