/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Controls.Suru2   2.4

Item {

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  property Item control
  property string backgroundColor: {
    if (control.highlighted)
        return control.Suru2.accentColor

    return control.down ? Qt.darker(Suru2.secondaryBackgroundColor, 1.1)
                        : Suru2.secondaryBackgroundColor
  }

  property bool backgroundVisible: control.down || control.hovered || control.highlighted
  property real backgroundOpacity: control.highlighted ? 0.5 : 1.0

  Rectangle {
    id: highlightRect

    width   : parent.width
    height  : parent.height
    visible : backgroundVisible
    opacity : backgroundOpacity
    color   : backgroundColor

    Behavior on color {
      ColorAnimation {
        duration    : control.Suru2.fastDuration
        easing.type : Easing.InCubic
      }
    }

    Behavior on opacity {
      NumberAnimation {
        duration    : control.Suru2.fastDuration
        easing.type : Easing.InCubic
      }
    }
  }

  Rectangle {
    width   : parent.width
    height  : parent.height
    visible : control.visualFocus
    color   : "transparent"

    border.width: 2
    border.color: control.Suru2.accentColor
  }
}
