#TEMPLATE = lib
#VERSION = 2.4

TARGET = qtquickcontrols2suru2styleplugin
QT += gui qml quick
#QT_PRIVATE += quick-private

#CONFIG += no_cxx_module install_qml_files builtin_resources qtquickcompiler

TARGETPATH = QtQuick/Controls.2/Suru2
uri = QtQuick.Controls.Suru2
#DESTDIR = QtQuick/Controls.2/Suru2
#TARGET = $$qtLibraryTarget($$TARGET)

DEFINES += INSTALL_PATH=\\\"$$[QT_INSTALL_QML]/$$TARGETPATH/\\\"

include(qmls.pri)

# Input
SOURCES += \
    $$PWD/src/qquicksuru2style.cpp \
    $$PWD/src/qtquickcontrols2suru2styleplugin.cpp \
    $$PWD/src/qquicksuru2icons.cpp \
    $$PWD/src/qquicksuru2units.cpp

HEADERS += \
    $$PWD/src/qquicksuru2style.h \
    $$PWD/src/qquicksuru2icons.h \
    $$PWD/src/qquicksuru2units.h

DISTFILES = \
    qmldir \
    $$QML_FILES

#OTHER_FILES = \
#    qmldir \
#    $$QML_FILES

RESOURCES += \
    $$PWD/suru2.qrc

#!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
#    copy_qmldir.target = $$OUT_PWD/qmldir
#    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
#    copy_qmldir.commands = $(COPY_FILE) "$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)" "$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)"
#    QMAKE_EXTRA_TARGETS += copy_qmldir
#    PRE_TARGETDEPS += $$copy_qmldir.target
#}

#qmldir.files = qmldir
#qml.files = $$QML_FILES

#unix {
#    installPath = $$[QT_INSTALL_QML]/$$TARGETPATH
##    qml.path    = $$installPath
##    qmldir.path = $$installPath
#    target.path = $$installPath
#    INSTALLS += target
#}

icons.files = $$PWD/icons/*
icons.path = $$[QT_INSTALL_QML]/$$TARGETPATH/icons
INSTALLS += icons


!static: CONFIG += qmlcache
CONFIG += no_cxx_module
load(qml_plugin)


#win32:createdir.commands = $(CHK_DIR_EXISTS) $$shell_path($$[QT_INSTALL_QML]/$$TARGETPATH/icons) & $(MKDIR) $$shell_path($$[QT_INSTALL_QML]/$$TARGETPATH/icons)
#linux:createdir.commands = $$shell_path($$[QT_INSTALL_QML]/$$TARGETPATH/icons) & $(MKDIR) $$shell_path($$[QT_INSTALL_QML]/$$TARGETPATH/icons)

## copies the given files to the destination directory
#defineTest(copyToDestDir) {
#    files = $$1
#    dir = $$2
#    # replace slashes in destination path for Windows
#    win32:dir ~= s,/,\\,g

#    for(file, files) {
#        # replace slashes in source path for Windows
#        win32:file ~= s,/,\\,g

#        QMAKE_POST_LINK += $$QMAKE_COPY_DIR $$shell_quote($$file) $$shell_quote($$dir) $$escape_expand(\\n\\t)
#    }

#    export(QMAKE_POST_LINK)
#}

#Use
#INSTALLS += copyToDestDir($$PWD/icons, $$[QT_INSTALL_QML]/$$TARGETPATH/icons)
