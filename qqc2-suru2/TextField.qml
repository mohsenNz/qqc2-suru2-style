/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls         2.4
import QtQuick.Controls.impl    2.4
import QtQuick.Controls.Suru2   2.4

T.TextField {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          placeholderText ? placeholder.implicitWidth + leftPadding + rightPadding : 0) ||
                 contentWidth + leftPadding + rightPadding

  implicitHeight: Math.max(contentHeight + topPadding + bottomPadding,
                           background ? background.implicitHeight : 0,
                           placeholder.implicitHeight + topPadding + bottomPadding)

  leftPadding     : control.Suru2.units.gu(1.5)
  rightPadding    : control.Suru2.units.gu(1.5)
  topPadding      : control.Suru2.units.gu(1)
  bottomPadding   : control.Suru2.units.gu(1)

  opacity: control.enabled ? 1.0 : 0.5

  color: control.Suru2.foregroundColor
  font: Suru2.units.fontParagraph

  selectionColor      : control.Suru2.accentColor
  selectedTextColor   : "white"
  verticalAlignment   : TextInput.AlignVCenter

  cursorDelegate: CursorDelegate { }

  PlaceholderText {
    id: placeholder

    x: control.leftPadding
    y: control.topPadding

    width: control.width - (control.leftPadding + control.rightPadding)
    height: control.height - (control.topPadding + control.bottomPadding)

    text    : control.placeholderText
    font    : control.font
    elide   : Text.ElideRight
    color   : control.Suru2.foregroundColor
    opacity : control.activeFocus ? 0.6 : 0.4
    visible : !control.length && !control.preeditText &&
              (!control.activeFocus || control.horizontalAlignment !== Qt.AlignHCenter)

    verticalAlignment: control.verticalAlignment
//        Suru.textLevel: control.Suru2.SecondaryText
  }

  background: Rectangle {
    implicitWidth: control.Suru2.units.gu(24)
    implicitHeight: control.Suru2.units.gu(4)

    radius: 4

    property string highlightColor: {
      switch (control.Suru2.highlightType) {
      case Suru2.Positive:
        return control.Suru2.positiveColor
      case Suru2.Negative:
        return control.Suru2.negativeColor
      case Suru2.Warning:
        return control.Suru2.warningColor
      case Suru2.Info:
        return control.Suru2.accentColor
      case Suru2.None:
        return control.Suru2.splitColor
      }
    }

    border.width: 1
    border.color: control.activeFocus ?
                    highlightColor :
                    control.Suru2.splitColor

    color: control.hovered && !control.activeFocus ?
             control.Suru2.secondaryBackgroundColor :
             control.Suru2.backgroundColor

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }
}
