/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

T.Slider {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

//    readonly property bool horizontal: control.orientation === Qt.Horizontal

  implicitWidth: horizontal ? control.Suru2.units.gu(32) : control.Suru2.units.gu(5)
  implicitHeight: horizontal ? control.Suru2.units.gu(5) : control.Suru2.units.gu(32)

  padding: control.Suru2.units.gu(1)
  opacity: control.enabled ? 1.0 : 0.5

  property bool useSystemFocusVisuals: true

  handle: Rectangle {
    implicitHeight: control.Suru2.units.gu(2)
    implicitWidth: control.Suru2.units.gu(2)

//        readonly property bool horizontal: control.orientation === Qt.Horizontal

    x: control.leftPadding +
       (control.horizontal ? control.visualPosition * (control.availableWidth - width) :
                             (control.availableWidth - width) / 2)

    y: control.topPadding +
       (control.horizontal ? (control.availableHeight - height) / 2 :
                             control.visualPosition * (control.availableHeight - height))

    //border.width: control.pressed ? 1 : 0
 //   border.color: control.Suru.neutralColor

    property string __focusColor: control.Suru2.theme === Suru2.Light ?
                                    control.Suru2.secondaryBackgroundColor :
                                    control.Suru2.tertiaryBackgroundColor

    property string __primaryColor: control.Suru2.theme === Suru2.Light ?
                                      control.Suru2.backgroundColor :
                                      control.Suru2.secondaryBackgroundColor

    radius  : 6
    color   : control.pressed ? Qt.darker(__focusColor, 1.1)
                              : control.hovered ? __focusColor : __primaryColor

    opacity : control.enabled ? 1.0 : 0.5


    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }

    layer.enabled: true
    layer.effect: ElevationEffect {
      elevation: !control.pressed ? 2 : 3
    }
  }

  background: Item {
    implicitWidth: horizontal ? control.Suru2.units.gu(5) : control.Suru2.units.gu(2)
    implicitHeight: horizontal ? control.Suru2.units.gu(2) : control.Suru2.units.gu(5)

    readonly property bool horizontal: control.orientation === Qt.Horizontal

    x: control.leftPadding + (control.horizontal ? 0 : (control.availableWidth - width) / 2)
    y: control.topPadding + (control.horizontal ? (control.availableHeight - height) / 2 : 0)

    width   : horizontal ? control.availableWidth : implicitWidth
    height  : horizontal ? implicitHeight : control.availableHeight
    scale   : horizontal && control.mirrored ? -1 : 1

    Rectangle {
      x: parent.horizontal ? 0 : (parent.width - width) / 2
      y: parent.horizontal ? (parent.height - height) / 2 : 0

      width   : parent.horizontal ? parent.width : 2
      height  : !parent.horizontal ? parent.height : 2
      color   : control.Suru2.tertiaryBackgroundColor
    }

    Rectangle {
      x: parent.horizontal ? 0 : (parent.width - width) / 2
      y: parent.horizontal ? (parent.height - height) / 2
                           : control.visualPosition * parent.height

      width   : parent.horizontal ? control.position * parent.width : 2
      height  : !parent.horizontal ? control.position * parent.height : 2
      color   : control.Suru2.accentColor
    }
  }
}
