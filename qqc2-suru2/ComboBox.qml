/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Window           2.3
import QtQuick.Controls         2.4
import QtQuick.Controls.impl    2.4
import QtQuick.Templates        2.4     as T
import QtGraphicalEffects       1.0
import QtQuick.Controls.Suru2   2.4

T.ComboBox {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  property bool useSystemFocusVisuals: true

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           Math.max(contentItem.implicitHeight, indicator ? indicator.implicitHeight : 0) +
                           topPadding + bottomPadding)

  baselineOffset: contentItem.y + contentItem.baselineOffset

  spacing       : control.Suru2.units.gu(2)
  topPadding    : control.Suru2.units.gu(0.5)
  bottomPadding : control.Suru2.units.gu(0.5)
  leftPadding   : control.Suru2.units.gu(2)
  rightPadding  : control.Suru2.units.gu(2)

  opacity : enabled ? 1.0 : 0.5
  font    : Suru2.units.fontParagraph

  delegate: MenuItem {
    id: menuItem

    width   : control.popup.width
    height  : control.Suru2.units.gu(4) + control.topPadding + control.bottomPadding
    text    : control.textRole ?
                (Array.isArray(control.model) ?
                   modelData[control.textRole] :
                   model[control.textRole]) : modelData

    highlighted: control.highlightedIndex === index

    hoverEnabled: control.hoverEnabled
    checked: control.currentIndex === index

    indicator: IconImage {

      sourceSize.width: parent.height / 3
      sourceSize.height: parent.height / 3

      x: menuItem.text ? (menuItem.mirrored ?
                   menuItem.width - width - menuItem.rightPadding : menuItem.leftPadding) :
                menuItem.leftPadding + (menuItem.availableWidth - width) / 2

      y: menuItem.topPadding + (menuItem.availableHeight - height) / 2

      color: control.Suru2.accentColor
      visible: menuItem.checkable && menuItem.checked
      source: "qrc:/assets/checkmark.svg"
    }
  }

  indicator: IconImage {
    id: img

    x: control.mirrored ? control.leftPadding : control.width - width - control.rightPadding
    y: control.topPadding + (control.availableHeight - height) / 2

    width   : control.Suru2.units.gu(2)
    height  : control.Suru2.units.gu(2)
    source  : "qrc:/assets/down.svg"
    color   : control.Suru2.foregroundColor

    sourceSize.width    : width
    sourceSize.height   : height

  }

  contentItem: T.TextField {
    leftPadding: control.mirrored &&
                 control.indicator ? control.indicator.width + control.spacing : 0

    rightPadding: !control.mirrored &&
                  control.indicator ? control.indicator.width + control.spacing : 0

    text: control.editable ? control.editText : control.displayText

    enabled             : control.editable
    autoScroll          : control.editable
    readOnly            : control.down
    inputMethodHints    : control.inputMethodHints
    validator           : control.validator
    font                : control.font
    color               : control.Suru2.foregroundColor
    selectionColor      : control.Suru2.secondaryBackgroundColor
    selectedTextColor   : control.Suru2.accentColor

    verticalAlignment: Text.AlignVCenter

    cursorDelegate: CursorDelegate { }

  }

  background: Rectangle {
    implicitWidth: Math.max(control.Suru2.units.gu(20), parent.width * 0.45)
    implicitHeight: control.Suru2.units.gu(4)

    radius: 4
    smooth: true

    border.width: control.flat ? 0 : 1

    border.color: control.pressed ||
                  popup.visible ||
                  control.visualFocus ?
                    control.Suru2.accentColor :
                    control.Suru2.splitColor

    color: control.pressed || popup.visible || control.hovered ?
             control.Suru2.secondaryBackgroundColor :
               control.Suru2.backgroundColor

    visible: !control.flat || control.pressed || control.hovered || control.visualFocus


    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }

  popup: T.Popup {
    width: control.width
    height: Math.min(contentItem.implicitHeight,
                     control.Window.height - topMargin - bottomMargin )

    implicitHeight: Math.min(control.Suru2.units.gu(48), contentItem.implicitHeight)

    topMargin   : control.Suru2.units.gu(1)
    bottomMargin: control.Suru2.units.gu(1)

    contentItem: ListView {
      clip: true
      implicitHeight: contentHeight
      model: control.popup.visible ? control.delegateModel : null
      currentIndex: control.highlightedIndex
      highlightRangeMode: ListView.ApplyRange
      highlightMoveDuration: 0
      interactive: false
      T.ScrollIndicator.vertical: ScrollIndicator { }
    }

    background: Rectangle {
      color: control.Suru2.secondaryBackgroundColor
      radius: 4

      layer.enabled: true
      layer.effect: ElevationEffect {
        elevation: 3
      }
    }
  }
}
