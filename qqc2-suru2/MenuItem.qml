/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Controls         2.4
import QtQuick.Controls.impl    2.4
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

T.MenuItem {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }


  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           Math.max(contentItem.implicitHeight, indicator ? indicator.implicitHeight : 0) +
                           topPadding + bottomPadding)

  baselineOffset  : contentItem.y + contentItem.baselineOffset

  leftPadding     : control.Suru2.units.gu(2)
  rightPadding    : control.Suru2.units.gu(2)
  topPadding      : control.Suru2.units.gu(2) - 1
  bottomPadding   : control.Suru2.units.gu(2) - 1

  spacing : control.Suru2.units.gu(2)

  icon.width  : 24
  icon.height : 24
  icon.color  : highlighted ? control.Suru2.accentColor : control.Suru2.foregroundColor

  opacity : control.enabled ? 1.0 : 0.5
  font    : Suru2.units.fontParagraph

  contentItem: IconLabel {
    readonly property real arrowPadding: control.subMenu && control.arrow ?
                                           control.arrow.width + control.spacing : 0

    readonly property real indicatorPadding: control.checkable && control.indicator ?
                                               control.indicator.width + control.spacing : 0

    leftPadding: control.checkable && !control.mirrored ?
                   control.indicator.width + control.spacing : 0

    rightPadding: control.checkable && control.mirrored ?
                    control.indicator.width + control.spacing : 0

    spacing     : control.spacing
    mirrored    : control.mirrored
    display     : control.display
    alignment   : Qt.AlignLeft
    text        : control.text
    font        : control.font
    color       : control.checked ? control.Suru2.accentColor : control.Suru2.foregroundColor
    icon        : control.icon

  }

  indicator: CheckIndicator {
    x: text ? (control.mirrored ?
                 control.width - width - control.rightPadding : control.leftPadding) :
              control.leftPadding + (control.availableWidth - width) / 2

    y: control.topPadding + (control.availableHeight - height) / 2

    visible: control.checkable
    control: control
  }

  background: Rectangle {
    implicitWidth   : control.Suru2.units.gu(24)
    implicitHeight  : control.Suru2.units.gu(6)

//        color: control.Suru2.theme ===
//               control.Suru2.Light ? control.Suru2.backgroundColor
//                                     : control.Suru2.secondaryBackgroundColor

    property string __focusColor: control.Suru2.theme === Suru2.Light ?
                                    control.Suru2.secondaryBackgroundColor :
                                    control.Suru2.tertiaryBackgroundColor

    property string __primaryColor: control.Suru2.theme === Suru2.Light ?
                                      control.Suru2.backgroundColor :
                                      control.Suru2.secondaryBackgroundColor

    color: control.down ?
             Qt.darker(__focusColor, 1.1) :
             control.hovered || control.highlighted ?
               __focusColor : __primaryColor

//        HighlightFocusRectangle {
//            control : control
//            width   : parent.width
//            height  : parent.height
//        }

    Rectangle {
      anchors.bottom: parent.bottom
      width: parent.width
      height: 1
      color: control.Suru2.splitColor
      opacity: 0.4
    }
  }
}
