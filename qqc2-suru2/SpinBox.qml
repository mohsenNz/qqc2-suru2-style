/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtGraphicalEffects       1.0
import QtQuick.Controls.Suru2   2.4

T.SpinBox {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + 16 +
                          (up.indicator ? up.indicator.implicitWidth : 0) +
                          (down.indicator ? down.indicator.implicitWidth : 0))

  implicitHeight: Math.max(contentItem.implicitHeight + topPadding + bottomPadding,
                           background ? background.implicitHeight : 0,
                           up.indicator ? up.indicator.implicitHeight : 0,
                           down.indicator ? down.indicator.implicitHeight : 0)

  baselineOffset: contentItem.y + contentItem.baselineOffset

  font        : Suru2.units.fontParagraph
  padding     : control.Suru2.units.gu(0.5)
  spacing     : control.Suru2.units.gu(1.5)
  leftPadding : padding + (control.mirrored ? (up.indicator ? up.indicator.width : 0)
                                            : (down.indicator ? down.indicator.width : 0))

  rightPadding: padding - 4 + (control.mirrored ? (down.indicator ? down.indicator.width : 0)
                                                : (up.indicator ? up.indicator.width : 0))

  validator: IntValidator {
    locale  : control.locale.name
    bottom  : Math.min(control.from, control.to)
    top     : Math.max(control.from, control.to)
  }

  contentItem: TextInput {
    text    : control.textFromValue(control.value, control.locale)
    font    : control.font
    color   : control.Suru2.foregroundColor
    readOnly: !control.editable

    selectionColor: control.Suru2.accentColor
    selectedTextColor: "white"

    horizontalAlignment: Qt.AlignHCenter
    verticalAlignment: TextInput.AlignVCenter

    validator: control.validator
    inputMethodHints: control.inputMethodHints
  }

  up.indicator: Item {
    implicitWidth: control.Suru2.units.gu(3)

    height: parent.height

    x: control.mirrored ? 0 : parent.width - width

    Rectangle {
      x: 2; y: 4

      width   : parent.width - 4
      height  : parent.height - 8
      color   : control.activeFocus ?
                  control.Suru2.accentColor :
                  control.up.pressed ?
                    Qt.darker(control.Suru2.backgroundColor, 1.2) :
                    control.up.hovered ?
                      Qt.darker(control.Suru2.backgroundColor, 1.1) :
                      control.Suru2.backgroundColor

      visible : control.up.pressed || control.up.hovered
      opacity : control.activeFocus && !control.up.pressed ? 0.5 : 1.0


      Behavior on color {
        ColorAnimation {
          duration: control.Suru2.fastDuration
          easing.type: Easing.InCubic
        }
      }
    }

    Image {
      id: img

      anchors.centerIn: parent

      width   : control.Suru2.units.gu(2)
      height  : width
      source  : "qrc:/assets/" + (control.mirrored ? "left" : "right") + "arrow.svg"

      sourceSize.width: width
      sourceSize.height: height
    }

    ColorOverlay {
      anchors.fill: img

      source: img
      color: control.Suru2.foregroundColor
    }
  }

  down.indicator: Item {
    implicitWidth: control.Suru2.units.gu(3)

    height: parent.height

    x: control.mirrored ? parent.width - width : 0

    Rectangle {
      x: 2; y: 4

      width   : parent.width - 4
      height  : parent.height - 8
      color   : control.activeFocus ?
                  control.Suru2.accentColor :
                  control.down.pressed ?
                    Qt.darker(control.Suru2.backgroundColor, 1.2) :
                    control.down.hovered ?
                      Qt.darker(control.Suru2.backgroundColor, 1.1) :
                      control.Suru2.backgroundColor

      visible: control.down.pressed || control.down.hovered
      opacity: control.activeFocus && !control.down.pressed ? 0.5 : 1.0

      Behavior on color {
        ColorAnimation {
          duration: control.Suru2.fastDuration
          easing.type: Easing.InCubic
        }
      }
    }

    Image {
      id: img2

      anchors.centerIn: parent

      width   : control.Suru2.units.gu(2)
      height  : width
      source  : "qrc:/assets/" + (control.mirrored ? "right" : "left") + "arrow.svg"

      sourceSize.width: width
      sourceSize.height: height
    }

    ColorOverlay {
      anchors.fill: img2

      source: img2
      color: control.Suru2.foregroundColor
    }
}

  background: Rectangle {
    implicitWidth: control.Suru2.units.gu(20)
    implicitHeight: control.Suru2.units.gu(4)

    radius: 2

    border.width: 1
    border.color: control.activeFocus ?
                      control.Suru2.accentColor :
                      control.Suru2.splitColor

    color: control.hovered && !control.activeFocus ?
             control.Suru2.secondaryBackgroundColor : control.Suru2.backgroundColor

    Behavior on color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: Easing.InCubic
      }
    }
  }
}
