/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Controls         2.4
import QtQuick.Controls.impl    2.4
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

T.SwipeDelegate {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)

  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           Math.max(contentItem.implicitHeight,
                                    indicator ? indicator.implicitHeight
                                              : 0) +
                           topPadding + bottomPadding)

  baselineOffset: contentItem.y + contentItem.baselineOffset

  padding         : control.Suru2.units.gu(2)
  topPadding      : padding - 1
  bottomPadding   : padding - 1

  spacing: 12

  font: Suru2.units.fontParagraph

  icon.width  : 24
  icon.height : 24
  icon.color  : control.Suru2.foregroundColor

  swipe.transition: Transition { SmoothedAnimation { velocity: 3; easing.type: Easing.InOutCubic } }

  opacity: control.enabled ? 1.0 : 0.5

  contentItem: IconLabel {
    text    : control.text
    font    : control.font
    icon    : control.icon
    color   : control.Suru2.foregroundColor
    spacing : control.spacing
    mirrored: control.mirrored
    display : control.display

    alignment: control.display === IconLabel.IconOnly ||
               control.display === IconLabel.TextUnderIcon ?
                 Qt.AlignCenter : Qt.AlignLeft


  }

  background: Rectangle {
    implicitHeight: control.Suru2.units.gu(7)

    color: control.Suru2.backgroundColor

    HighlightFocusRectangle {
      control : control
      width   : parent.width
      height  : parent.height
    }

    Rectangle {
      anchors.bottom: parent.bottom

      width   : parent.width
      height  : 1
      color   : control.Suru2.secondarySplitColor
    }
  }
}
