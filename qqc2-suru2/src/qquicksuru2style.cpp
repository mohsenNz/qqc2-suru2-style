﻿#include "qquicksuru2style.h"
#include <QtQml/qqmlinfo.h>
#include <QtCore/QtMath>
#include <QtGui/QGuiApplication>

QQuickSuru2Style::QQuickSuru2Style(QObject *parent) :
  m_theme(Light),
  m_highlightType(None),
  m_devicePixelRatio(qGuiApp->devicePixelRatio())
{
  m_icons = new QQuickSuru2Icons();
  m_units = new QQuickSuru2Units(this);
}

QQuickSuru2Style *QQuickSuru2Style::qmlAttachedProperties(QObject *object)
{
  return new QQuickSuru2Style(object);
}

QQuickSuru2Style::Theme QQuickSuru2Style::theme() const
{
  return m_theme;
}

void QQuickSuru2Style::setTheme(QQuickSuru2Style::Theme theme)
{
  if (m_theme == theme)
    return;

  m_theme = theme;

  emit themeChanged();
  emit paletteChanged();
}

QQuickSuru2Style::HighlightType QQuickSuru2Style::highlightType() const
{
  return m_highlightType;
}

void QQuickSuru2Style::setHighlightType(const QQuickSuru2Style::HighlightType &h)
{
  if (m_highlightType == h)
    return;

  m_highlightType = h;
  emit highlightChanged();
}

QColor QQuickSuru2Style::overlayColor() const
{
  return alphaColor(black, m_theme == Light ? 0.75 : 0.62);
}

QColor QQuickSuru2Style::splitColor() const
{
  return alphaColor(m_theme == Light ? black : white, 0.3);
}

QColor QQuickSuru2Style::secondarySplitColor() const
{
  return alphaColor(m_theme == Light ? black : white, 0.1);
}

QColor QQuickSuru2Style::alphaColor(QColor color, qreal opacity) const
{
  QColor c = color;
  c.setAlphaF(opacity);

  return c;
}

