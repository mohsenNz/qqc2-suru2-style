#ifndef QQUICKSURU2STYLE_H
#define QQUICKSURU2STYLE_H

#include <QObject>
#include <QQmlEngine>
#include <QtGui/qcolor.h>

#include "qquicksuru2icons.h"
#include "qquicksuru2units.h"

#define SURU2_PALETTE_COLOR(name, color) \
  Q_PROPERTY(QColor name READ name NOTIFY themeChanged FINAL) \
  public: \
    QColor name() const { return QColor::fromRgb(colors[color][m_theme]); } \
  private:

static const QRgb black       = 0xFF000000;
static const QRgb jet         = 0xFF111111;
static const QRgb inkstone    = 0xFF292929;
static const QRgb slate       = 0xFF484848;
static const QRgb graphite    = 0xFF666666;
static const QRgb ash         = 0xFF888888;
static const QRgb silk        = 0xFFCDCDCD;
static const QRgb porcelain   = 0xFFEFEFEF;
static const QRgb white       = 0xFFFFFFFF;
static const QRgb lightBlue   = 0xFF19B6EE;
static const QRgb darkBlue    = 0xFF335280;
static const QRgb lightGreen  = 0xFF3eb34f;
static const QRgb darkGreen   = 0xFF0e8420;
static const QRgb lightCyan   = 0xFF00e676;
static const QRgb darkCyan    = 0xFF00c853;
static const QRgb lightRed    = 0xFFed3146;
static const QRgb darkRed     = 0xFFc7162b;
static const QRgb orange      = 0xFFe95420;

static const QRgb colors[][2] = {
// Light-Theme, Dark-Theme
  {darkGreen  , lightGreen},   // PositiveColor
  {darkRed    , lightRed  },   // NegativeColor
  {orange     , orange    },   // WarningColor
  {darkBlue   , lightBlue },   // AccentColor
  {darkCyan   , lightCyan },   // CheckColor
  {white      , jet       },   // BackgroundColor
  {porcelain  , inkstone  },   // SecondaryBackgroundColor
  {silk       , slate     },   // TertiaryBackgroundColor
  {ash        , graphite  },   // MediumColor
  {graphite   , ash       },   // HighColor
  {inkstone   , porcelain },   // SecondaryForegroundColor
  {black      , white     },   // ForegroundColor
};


class QQuickSuru2Style : public QObject
{
  Q_OBJECT

  // Palette Colors
  SURU2_PALETTE_COLOR(positiveColor             , PositiveColor             )
  SURU2_PALETTE_COLOR(negativeColor             , NegativeColor             )
  SURU2_PALETTE_COLOR(warningColor              , WarningColor              )
  SURU2_PALETTE_COLOR(accentColor               , AccentColor               )
  SURU2_PALETTE_COLOR(checkColor                , CheckColor                )
  SURU2_PALETTE_COLOR(backgroundColor           , BackgroundColor           )
  SURU2_PALETTE_COLOR(secondaryBackgroundColor  , SecondaryBackgroundColor  )
  SURU2_PALETTE_COLOR(tertiaryBackgroundColor   , TertiaryBackgroundColor   )
  SURU2_PALETTE_COLOR(mediumColor               , MediumColor               )
  SURU2_PALETTE_COLOR(highColor                 , HighColor                 )
  SURU2_PALETTE_COLOR(secondaryForegroundColor  , SecondaryForegroundColor  )
  SURU2_PALETTE_COLOR(foregroundColor           , ForegroundColor           )

  // Theme
  Q_PROPERTY(Theme theme READ theme WRITE setTheme NOTIFY themeChanged FINAL)

  // Highlight Type
  Q_PROPERTY(HighlightType highlightType READ highlightType WRITE setHighlightType 
      NOTIFY highlightChanged FINAL)

  // Alpha Colors
  Q_PROPERTY(QColor overlayColor        READ overlayColor NOTIFY paletteChanged FINAL)
  Q_PROPERTY(QColor secondarySplitColor READ secondarySplitColor NOTIFY paletteChanged FINAL)
  Q_PROPERTY(QColor splitColor          READ splitColor NOTIFY paletteChanged FINAL)

  // Animation Speeds
  Q_PROPERTY(int snapDuration     READ snapDuration   CONSTANT)
  Q_PROPERTY(int fastDuration     READ fastDuration   CONSTANT)
  Q_PROPERTY(int briskDuration    READ briskDuration  CONSTANT)
  Q_PROPERTY(int slowDuration     READ slowDuration   CONSTANT)
  Q_PROPERTY(int sleepyDuration   READ sleepyDuration CONSTANT)

  Q_PROPERTY(QQuickSuru2Icons* icons READ icons CONSTANT)
  Q_PROPERTY(QQuickSuru2Units* units READ units CONSTANT)

public:
  enum Theme {
    Light, Dark
  };
  Q_ENUM(Theme)

  //Q_ENUM(Color)
  enum Color {
    PositiveColor, NegativeColor, WarningColor, AccentColor,
    CheckColor, BackgroundColor, SecondaryBackgroundColor, TertiaryBackgroundColor,
    MediumColor, HighColor, SecondaryForegroundColor, ForegroundColor
  };

  enum HighlightType {
    Positive, Negative,
    Warning, Info, None
  };
  Q_ENUM(HighlightType)

  explicit QQuickSuru2Style(QObject *parent = nullptr);
  static QQuickSuru2Style *qmlAttachedProperties(QObject *object);

  Theme theme() const;
  void setTheme(Theme theme);

  HighlightType highlightType() const;
  void setHighlightType(const HighlightType &h);

  QColor overlayColor() const;
  QColor splitColor() const;
  QColor secondarySplitColor() const;

  int snapDuration() const    { return 100; }
  int fastDuration() const    { return 165; }
  int briskDuration() const   { return 333; }
  int slowDuration() const    { return 500; }
  int sleepyDuration() const  { return 1000; }

  QQuickSuru2Icons* icons() const { return m_icons; }
  QQuickSuru2Units* units() const { return m_units; }

Q_SIGNALS:
  void themeChanged();
  void highlightChanged();
  void paletteChanged();

private:
  QColor alphaColor(QColor alphaColor, qreal opacity = 1.0) const;
  QColor paletteColor(int c) const { return QColor::fromRgb(colors[c][m_theme]); }

  // The actual values for this item, whether explicit, or globally set.
  Theme m_theme = Dark;

  HighlightType m_highlightType;

  float m_devicePixelRatio;
  float m_gridUnit;

  QQuickSuru2Icons* m_icons;
  QQuickSuru2Units* m_units;

};
QML_DECLARE_TYPEINFO(QQuickSuru2Style, QML_HAS_ATTACHED_PROPERTIES)

#endif // QQUICKSURU2STYLE_H
