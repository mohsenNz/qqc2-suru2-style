#include "qquicksuru2units.h"

#include <QtCore/QtMath>
#include <QtGui/qfontinfo.h>
#include <QtGui/QGuiApplication>
#include <QFontDatabase>
//#include <QtGui/private/qhighdpiscaling_p.h>

#define SURU_FONT_SIZE_MULTIPLIER 0.875
#define ENV_GRID_UNIT_PX "GRID_UNIT_PX"
#define DEFAULT_GRID_UNIT_PX 8

//static float getenvFloat(const char* name, float defaultValue)
//{
//    QByteArray stringValue = qgetenv(name);
//    bool ok;
//    float value = stringValue.toFloat(&ok);
//    return ok ? value : defaultValue;
//}

QQuickSuru2Units::QQuickSuru2Units(QObject *parent) :
    QObject(parent),
    m_devicePixelRatio(qGuiApp->devicePixelRatio()),
    m_fontNotFond(false)
{
    // TODO recalculate based on window/devicePixelRatio changes like the ubuntu uitk
//    if (QHighDpiScaling::isActive()) {
    m_gridUnit = qCeil(DEFAULT_GRID_UNIT_PX * m_devicePixelRatio);
//    }
//    else {
//        m_gridUnit = getenvFloat(ENV_GRID_UNIT_PX, DEFAULT_GRID_UNIT_PX * m_devicePixelRatio);
//    }

    setupFonts();
}

void QQuickSuru2Units::setupFonts()
{
    m_headingOneCondensed.setPixelSize(dp(32) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingOneCondensed.setWeight(QFont::Normal);

    m_headingTwoCondensed.setPixelSize(dp(24) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingTwoCondensed.setWeight(QFont::Normal);

    m_headingThreeCondensed.setPixelSize(dp(22) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingThreeCondensed.setWeight(QFont::Normal);

    m_headingOneNormal.setPixelSize(dp(32) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingOneNormal.setWeight(QFont::Normal);

    m_headingTwoNormal.setPixelSize(dp(24) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingTwoNormal.setWeight(QFont::Normal);

    m_headingThreeNormal.setPixelSize(dp(22) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingThreeNormal.setWeight(QFont::Normal);

    m_headingOne.setPixelSize(dp(32) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingOne.setWeight(QFont::Light);

    m_headingTwo.setPixelSize(dp(24) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingTwo.setWeight(QFont::Light);

    m_headingThree.setPixelSize(dp(22) * SURU_FONT_SIZE_MULTIPLIER);
    m_headingThree.setWeight(QFont::Light);

    m_paragraph.setPixelSize(dp(16) * SURU_FONT_SIZE_MULTIPLIER);
    m_paragraph.setWeight(QFont::Light);

    m_small.setPixelSize(dp(14) * SURU_FONT_SIZE_MULTIPLIER);
    m_small.setWeight(QFont::Light);

    m_caption.setPixelSize(dp(14) * SURU_FONT_SIZE_MULTIPLIER);
    m_caption.setWeight(QFont::Light);
    m_caption.setItalic(true);

    m_codeBlock.setPixelSize(dp(16) * SURU_FONT_SIZE_MULTIPLIER);
    m_codeBlock.setWeight(QFont::Light);

//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-L.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-LI.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-R.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-RI.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-M.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-MI.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-B.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-BI.ttf");

//    QFontDatabase::addApplicationFont(":/fonts/UbuntuMono-R.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/UbuntuMono-RI.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/UbuntuMono-B.ttf");
//    QFontDatabase::addApplicationFont(":/fonts/UbuntuMono-BI.ttf");

//    QFontDatabase::addApplicationFont(":/fonts/Ubuntu-C.ttf");

    const QFont font(QStringLiteral("Ubuntu"));
    if (QFontInfo(font).family() == QStringLiteral("Ubuntu")) {
        const QString family = font.family();

        m_headingOneNormal.setFamily(family);
        m_headingTwoNormal.setFamily(family);
        m_headingThreeNormal.setFamily(family);
        m_headingOne.setFamily(family);
        m_headingTwo.setFamily(family);
        m_headingThree.setFamily(family);
        m_paragraph.setFamily(family);
        m_small.setFamily(family);
        m_caption.setFamily(family);
    } else {
        m_fontNotFond = true;
    }

    const QFont monoFont(QStringLiteral("Ubuntu Mono"));
    if (QFontInfo(monoFont).family() == QStringLiteral("Ubuntu Mono")) {
        const QString family = monoFont.family();
        m_codeBlock.setFamily(family);
    } else {
        m_fontNotFond = true;
    }

    const QFont condensedFont(QStringLiteral("Ubuntu Condensed"));
    if (QFontInfo(condensedFont).family() == QStringLiteral("Ubuntu Condensed")) {
        const QString family = condensedFont.family();
        m_headingOneCondensed.setFamily(family);
        m_headingTwoCondensed.setFamily(family);
        m_headingThreeCondensed.setFamily(family);
    } else {
        m_fontNotFond = true;
    }


    emit fontsChanged();
}



int QQuickSuru2Units::gu(qreal value) const
{
    return qRound(value * m_gridUnit) / m_devicePixelRatio;
}

int QQuickSuru2Units::dp(qreal value) const
{
//    if (QHighDpiScaling::isActive()) {
//        return value;
//    }
//    else {
//        const qreal ratio = m_gridUnit / DEFAULT_GRID_UNIT_PX;
//        if (value <= 2.0) {
//            // for values under 2dp, return only multiples of the value
//            return qRound(value * qFloor(ratio)) / m_devicePixelRatio;
//        } else {
//            return qRound(value * ratio) / m_devicePixelRatio;
//        }
//    }
    return value;
}

int QQuickSuru2Units::rem(qreal value) const
{
    return value * qreal(m_paragraph.pixelSize());
}

void QQuickSuru2Units::registerFonts()
{
    if (m_fontNotFond) {
        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-L.ttf");
        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-LI.ttf");
        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-R.ttf");
        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-RI.ttf");
        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-M.ttf");
        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-MI.ttf");
        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-B.ttf");
        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-BI.ttf");

        QFontDatabase::addApplicationFont(":/fonts/UbuntuMono-R.ttf");
        QFontDatabase::addApplicationFont(":/fonts/UbuntuMono-RI.ttf");
        QFontDatabase::addApplicationFont(":/fonts/UbuntuMono-B.ttf");
        QFontDatabase::addApplicationFont(":/fonts/UbuntuMono-BI.ttf");

        QFontDatabase::addApplicationFont(":/fonts/Ubuntu-C.ttf");

        setupFonts();
    }

}
