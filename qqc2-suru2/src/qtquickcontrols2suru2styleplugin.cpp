#include <QQmlExtensionPlugin>
#include <qqml.h>
#include "qquicksuru2style.h"
#include "qquicksuru2icons.h"

#include <QDebug>

class QtQuickControls2Suru2StylePlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;

private:
    QUrl resolvedUrl(const QString &fileName) const;
};

void QtQuickControls2Suru2StylePlugin::registerTypes(const char *uri)
{
    // @uri QtQuick.Controls.Suru2

    qmlRegisterModule(uri, 2, QT_VERSION_MINOR - 7); // Qt 5.11->2.4, 5.12->2.5...
    qmlRegisterType<QQuickSuru2Icons>();
    qmlRegisterUncreatableType<QQuickSuru2Style>(uri, 2, 0, "Suru2", "Suru is an attached property");

//    QByteArray import = QByteArray(uri) + ".impl";
//    qmlRegisterModule(import, 2, QT_VERSION_MINOR - 7); // Qt 5.11->2.4, 5.12->2.5...

//    qDebug() << "1374_INSTALL_PATH!: " << INSTALL_PATH;
//    qDebug() << "1374_resolvedUrl!: " <<  resolvedUrl("CheckIndicator.qml");

//    qmlRegisterType(resolvedUrl("CheckIndicator.qml"), import, 2, 0, "CheckIndicator");
}


QUrl QtQuickControls2Suru2StylePlugin::resolvedUrl(const QString &fileName) const
{
    QString installPath = QString::fromStdString(INSTALL_PATH);
    return QUrl::fromLocalFile(installPath.append(fileName));
}



#include "qtquickcontrols2suru2styleplugin.moc"

