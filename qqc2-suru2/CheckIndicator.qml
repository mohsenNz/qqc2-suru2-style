/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Controls         2.4
import QtQuick.Controls.impl    2.4
import QtGraphicalEffects       1.0
import QtQuick.Controls.Suru2   2.4

Item {

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth   : control.Suru2.units.gu(2.5)
  implicitHeight  : control.Suru2.units.gu(2.5)

  property Item control

  Rectangle {
    anchors.fill: parent
    color: control.Suru2.backgroundColor

    border.color: control.down ?
                    (control.checked ?
                      "transparent" : control.Suru2.checkColor) :
                    control.checked ?
                      control.Suru2.checkColor : control.Suru2.splitColor

    border.width: 1

    radius: 4
    scale: control.down ? 0.91 : 1.0

    Rectangle {
      anchors.fill: parent

      radius  : parent.radius
      scale   : control.checked ? 1.0 : 0.0

      color   : control.Suru2.checkColor

      Behavior on scale {
        NumberAnimation {
          duration: control.Suru2.snapDuration
          easing.type: control.down ?
                         Easing.InCubic : Easing.OutCubic

        }
      }
    }

    Behavior on scale {
      NumberAnimation {
        duration: control.Suru2.snapDuration
        easing.type: control.down ?
                       Easing.InCubic : Easing.OutCubic

      }
    }

    Behavior on border.color {
      ColorAnimation {
        duration: control.Suru2.fastDuration
        easing.type: control.down || control.checked ?
                       Easing.InCubic : Easing.OutCubic

      }
    }
  }

  IconImage {
    id: indicator

    anchors { fill: parent; margins: 2 }

    source: control.checkState === Qt.PartiallyChecked ?
              "qrc:/assets/checkmark-undefined.svg" : "qrc:/assets/checkmark.svg"

    sourceSize.width: width
    sourceSize.height: height

    visible: control.checkState !== Qt.Unchecked
    color: control.Suru2.backgroundColor
  }

}
