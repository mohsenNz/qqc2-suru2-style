/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.11
import QtQuick.Templates        2.4     as T
import QtQuick.Controls.Suru2   2.4

Rectangle {
  Suru2.theme: control.Suru2.theme
  Suru2.highlightType: control.Suru2.highlightType

  implicitWidth   : control.Suru2.units.gu(2.5)
  implicitHeight  : control.Suru2.units.gu(2.5)

  property Item control

  radius  : width / 2
  color   : control.Suru2.backgroundColor
  scale   : control.down ? 0.91 : 1.0

  border.color: control.down ?
                  (control.checked ? "transparent" : control.Suru2.checkColor) :
                  control.checked ? control.Suru2.checkColor : control.Suru2.splitColor

  border.width: 1

  Rectangle {
    anchors.fill: parent

    radius  : parent.radius
    scale   : control.checked ? 1.0 : 0.0
    color   : control.Suru2.checkColor

    Behavior on scale {
      NumberAnimation {
        duration: control.Suru2.snapDuration
        easing.type: Easing.InCubic
      }
    }

    Rectangle {
      anchors.centerIn: parent

      width   : control.Suru2.units.gu(1)
      height  : width
      radius  : width * 0.5
      color   : control.Suru2.backgroundColor
      visible : control.checked
      opacity : visible ? 1.0 : 0.0

      Behavior on opacity {
        NumberAnimation {
          duration: control.Suru2.fastDuration
          easing.type: Easing.InCubic
        }
      }
    }
  }

  Behavior on scale {
    NumberAnimation {
      duration: control.Suru2.snapDuration
      easing.type: Easing.InCubic
    }
  }

  Behavior on color {
    ColorAnimation {
      duration: control.Suru2.fastDuration
      easing.type: Easing.InCubic
    }
  }

  Behavior on border.color {
    ColorAnimation {
      duration: control.Suru2.fastDuration
      easing.type: Easing.InCubic
    }
  }
}
