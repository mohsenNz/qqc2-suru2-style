/****************************************************************************
**
** Copyright (C) 2017, 2018 Stefano Verzegnassi <stefano@ubports.com>
** Copyright (C) 2017 The Qt Company Ltd.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
****************************************************************************/

import QtQuick                  2.12
import QtQuick.Templates        2.12     as T
import QtQuick.Controls         2.12
import QtQuick.Controls.Suru2   2.4

T.DialogButtonBox {
  id: control

  Suru2.theme: {
    try { return suru2Conf.Suru2.theme }
    catch (err) { return Suru2.Light }
  }

  Suru2.highlightType: {
    try { return suru2Conf.Suru2.highlightType }
    catch (err) { return Suru2.None }
  }

  implicitWidth: Math.max(background ? background.implicitWidth : 0,
                          contentItem.implicitWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(background ? background.implicitHeight : 0,
                           contentItem.implicitHeight + topPadding + bottomPadding)

  contentWidth: width - leftPadding - rightPadding
  contentHeight: height - topPadding - bottomPadding

  spacing     : control.Suru2.units.gu(1)
  padding     : control.Suru2.units.gu(2)
  topPadding  : position === T.DialogButtonBox.Footer ?
                  control.Suru2.units.gu(1) : control.Suru2.units.gu(2)

  bottomPadding: position === T.DialogButtonBox.Header ?
                   control.Suru2.units.gu(1) : control.Suru2.units.gu(2)

//    alignment: Qt.AlignRight
  alignment: Qt.AlignHCenter

  delegate: Button {
//        width: implicitWidth//control.count === 1 ? control.availableWidth / 2 : undefined

//        highlighted: true
    width: {
      var dialogWidth = control.width - control.leftPadding - control.rightPadding
      if (contentItemView.count === 2) {
        return Math.max(implicitWidth, (dialogWidth - control.spacing) / 2)
      } else {
        return dialogWidth
      }
    }

//        flat                : true
//        font.capitalization : Font.AllUppercase
//        font.weight         : Font.Normal

//        Suru2.highlightType : Suru2.Positive

    Suru2.highlightType: {
      switch (DialogButtonBox.buttonRole) {
      case DialogButtonBox.AcceptRole:
      case DialogButtonBox.YesRole:
      case DialogButtonBox.ApplyRole:
        return Suru2.Positive
      case DialogButtonBox.RejectRole:
      case DialogButtonBox.NoRole:
      case DialogButtonBox.DestructiveRole:
        return Suru2.Negative
      default:
        return Suru2.None
      }
    }

  }

  contentItem: ListView {
    id: contentItemView

    implicitWidth   : contentWidth
    implicitHeight  : count < 3 ? control.Suru2.units.gu(4) : contentHeight
    contentWidth    : control.width - control.leftPadding - control.rightPadding

    model: control.contentModel

    verticalLayoutDirection: ListView.BottomToTop

    spacing         : count < 3 ? control.spacing : control.Suru2.units.gu(2)
    orientation     : count < 3 ? ListView.Horizontal : ListView.Vertical
    boundsBehavior  : Flickable.StopAtBounds
    snapMode        : ListView.SnapToItem
  }
}
