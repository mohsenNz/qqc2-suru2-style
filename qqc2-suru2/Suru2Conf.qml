import QtQuick                  2.11
import QtQuick.Controls.Suru2   2.4

Item {
  Suru2.theme: Suru2.Light
  Suru2.highlightType : Suru2.None

  Component.onCompleted: {
    Suru2.units.registerFonts()
  }
}
